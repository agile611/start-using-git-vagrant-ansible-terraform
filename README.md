[![Agile611](https://www.agile611.com/wp-content/uploads/2020/09/cropped-logo-header.png)](http://www.agile611.com/)

# Curso Git Vagrant Ansible Terraform

Este repositorio contiene 4 submodules

* [Git](https://bitbucket.org/agile611/startusinggit)
* [Vagrant](https://bitbucket.org/agile611/startusingvagrant)
* [Ansible](https://bitbucket.org/agile611/startusingansible)
* [Terraform](https://bitbucket.org/agile611/startusingterraform)


Para sincronizar todo, sigue estos comandos en un terminal:

```shell
    git clone https://bitbucket.org/agile611/start-using-git-vagrant-ansible-terraform.git
    cd start-using-git-vagrant-ansible-terraform
    git submodule update --init --recursive
```

A partir de aquí, ya podemos empezar a usar el código que tenemos en estas partes

## Common networking problems

If you have proxies or VPNs running on your machine, it is possible that Vagrant is not able to provision your environment.

Please check your connectivity before.

## Support

This tutorial is released into the public domain by [Agile611](http://www.agile611.com/) under Creative Commons Attribution-NonCommercial 4.0 International.

[![License: CC BY-NC 4.0](https://img.shields.io/badge/License-CC_BY--NC_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc/4.0/)


This README file was originally written by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhs/) and is likewise released into the public domain.

Please contact Agile611 for further details.

* [Agile611](http://www.agile611.com/)
* Laureà Miró 309
* 08950 Esplugues de Llobregat (Barcelona)